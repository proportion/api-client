# Summary

Sample implementation of the Abacus API for the guest lecture at the OST on 22nd of May 2023.
The project consists of a very basic HTML file (index.html) with some styling (styles.css).

Heart of the project is the script.js which fetches address records from the Abacus API running locally without any 
authentication.

The next step is using the OAuth Client Credentials flow in order to access the public development server using a 
client ID and client secret. 
For presentation purposes only, do not use it in production as the client credentials need to be kept secret.