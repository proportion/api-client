const url = 'https://entity-api1-1.demo.abacus.ch';
const apiEndpoint = url + '/api/entity/v1/mandants/7777/Subjects?$orderby=Name';
const tokenEndpoint = url + '/oauth/oauth2/v1/token';
const clientId = '840a12c0-5a3a-1fed-0501-2252c1251036';
const clientSecret = 'd5282c63-c71f-7ff6-e5c6-0ef5f6685e3b';

document.getElementById('retrieve').addEventListener('click', fetchAndDisplayAddresses);

function fetchAndDisplayAddresses() {
    retrieveAccessToken()
        .then(accessToken => {
            console.log(accessToken);
            const options = {
                method: 'GET', headers: {
                    'Authorization': 'Bearer ' + accessToken
                }
            };

            fetch(apiEndpoint, options)
                .then(response => response.json())
                .then(displayAddresses)
                .catch(error => console.error('Error:', error));
        })
        .catch(error => console.error('Error:', error));
}

function retrieveAccessToken() {
    const requestOptions = {
        method: 'POST', headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic ' + btoa(clientId + ':' + clientSecret)
        }, body: 'grant_type=client_credentials'
    };

    return fetch(tokenEndpoint, requestOptions)
        .then(response => response.json())
        .then(data => data.access_token);
}

function displayAddresses(data) {
    const addressesTable = document.getElementById('addresses');
    addressesTable.innerHTML = '';  // Clear previous addresses

    // Create table headers
    const headers = ['FirstName', 'Name', 'Type'];
    const tableHead = addressesTable.createTHead();
    const headerRow = tableHead.insertRow();
    for (let header of headers) {
        const th = document.createElement('th');
        th.textContent = header;
        headerRow.appendChild(th);
    }

    // Insert address rows
    data.value.forEach(address => {
        const row = addressesTable.insertRow();
        for (let field of headers) {
            let cell = row.insertCell();
            cell.textContent = address[field];
        }
    });
}
