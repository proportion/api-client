// 1. add a listener to the button on the website
document.getElementById('retrieve').addEventListener('click', fetchAndDisplayAddresses);

// 2. implement a function which
// - fetches the content from the API endpoint
// - extracts the json data from the response
// - displays the data
// - catches any error and logs it on the console
function fetchAndDisplayAddresses() {
    fetch("http://localhost:8081/api/entity/v1/mandants/7777/Subjects?$orderby=Name")
        .then(response => response.json())
        .then(displayAddresses)
        .catch(error => console.error('Error:', error));
}

// 3. display the addresses data by
// - getting the table element and clearing its HTML content
// - populate the table header with the column FirstName, Name, Type
// - add the rows for the json data
function displayAddresses(data) {
    const addressesTable = document.getElementById('addresses');
    addressesTable.innerHTML = '';  // Clear previous addresses

    // Create table headers
    const headers = ['FirstName', 'Name', 'Type'];
    const tableHead = addressesTable.createTHead();
    const headerRow = tableHead.insertRow();
    for (let header of headers) {
        const th = document.createElement('th');
        th.textContent = header;
        headerRow.appendChild(th);
    }

    // insert address rows
    data.value.forEach(address => {
        const row = addressesTable.insertRow();
        for (let field of headers) {
            let cell = row.insertCell();
            cell.textContent = address[field];
        }
    });
}